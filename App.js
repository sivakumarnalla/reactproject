import React, { Suspense, lazy, useEffect, useState } from "react";
import  ReactDOM  from "react-dom/client";
import Body from "./src/components/Body";
import Header from "./src/components/Header";
import { Outlet, RouterProvider, createBrowserRouter } from "react-router-dom";
import About from "./src/components/About";
import Contact from "./src/components/Contact";
import Error from "./src/components/Error";
import ProductMenu from "./src/components/ProductMenu";
import UserContext from "./src/components/utils/UserContext";
import { Provider } from "react-redux";
import appStore from "./src/components/utils/appStore";
import Cart from "./src/components/Cart";
//import Grocery from "./src/components/Grocery";



const Grocery = lazy(() => import("./src/components/Grocery"))
const AppLayout = () =>{

    const [userName,setUserName] = useState();
        useEffect(()=>{
            const data = {
                name: "Siva Kumar",
            }
            setUserName(data.name);
        },[]);
    return(
        <Provider store={appStore}>
        <UserContext.Provider value={{loggedInUser:userName, setUserName}}>
        <div className="app">
            <Header/>
            <Outlet/>
        </div>
        </UserContext.Provider>
        </Provider>
    )
};
const appRouter = createBrowserRouter([
    {
        path:"/",
        element:<AppLayout/>,
        children : [
            {
                path: "/",
                element: <Body/>
            },
            {
                path: "/about",
                element: <About/>
            },
            {
                path : "/contact",
                element : <Contact/>,
            },
            {
                path : "/grocery",
                element : <Suspense fallback ={<h1>Loading...</h1>}>
                    <Grocery/>
                </Suspense>
            },
            {
                path : "/restaurant/:id",
                element : <ProductMenu/>,
            },
            {
                path :"/cart",
                element : <Cart/>
            }
        ],
        errorElement: <Error />,
    },
    
])

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<RouterProvider router={appRouter} />);

       