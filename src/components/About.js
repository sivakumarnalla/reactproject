import React from "react";
import UserClass from "./UserClass";
import UserContext from "./utils/UserContext";

class About extends React.Component{
    constructor(props){
        super(props)
       // console.log("parent Constructor");
    }
    componentDidMount(){
       // console.log("parent ComponentDidMount")
    }
    render(){
        //console.log("parent Render")
        return(
            <div>
                <h1>About</h1>
                <h2>This is About Page here</h2>
                <div>
                    Logged In User 
                    <UserContext.Consumer>
                       { ({loggedInUser}) => (
                        <h1 className="text-xl font-bold">{loggedInUser}</h1>
                       )}
                    </UserContext.Consumer>
                </div>
                <UserClass name = {"Siva Kumar Nalla"}/>
            </div>
        )
    }
}



export default About;