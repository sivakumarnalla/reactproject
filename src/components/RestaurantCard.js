


const RestaurantCard = (props) => {

    const {resData} = props;
    //console.log(resData);

    const {
        name,
        avgRating,
        cuisines,
        costForTwo,
        cloudinaryImageId,
       
    } = resData.info;   

    return(
        <div data-testid ="resCard"className="m-4 p-4 w-52 rounded-lg bg-gray-200">
            <img 
                className="w-full" alt="res-logo"
                src={"https://media-assets.swiggy.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_660/" + cloudinaryImageId}/>
                <h3 className="font-bold">{name}</h3>
                <h4>{avgRating} Stars</h4>
                <h4>{cuisines.join(",")}</h4>
                <h4>{costForTwo} </h4>
                
        </div>
    )
}

export const withPromotedLabel = (RestaurantCard) =>{
    return (props) =>{
        return (
            <div>
                <label className=" absolute m-2 p-2 bg-black text-white rounded-lg">
                    Promoted
                </label>
                <RestaurantCard{...props}/>
            </div>
        );
    };
};

export default RestaurantCard