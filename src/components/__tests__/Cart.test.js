import { fireEvent, render, screen } from "@testing-library/react"
import { act } from "react-dom/test-utils"
import ProductMenu from "../ProductMenu"
import MOCK_DATA from "../mocks/resMenuMock.json"
import { Provider } from "react-redux"
import appStore from "../utils/appStore"

global.fetch = jest.fn(() =>
    Promise.resolve({
        json: ()=>Promise.resolve(MOCK_DATA),
    })
)
it("Should Load Restaurant  Menu Component", async() =>{
    await act(async () => render(
    <Provider store={appStore}>
    <ProductMenu/>
    </Provider>
    ));

    const accordionHeader = screen.getByText("10 Members Package (4)");

    fireEvent.click(accordionHeader)
    expect(screen.getAllByTestId("foodItems").length).toBe(4);
});