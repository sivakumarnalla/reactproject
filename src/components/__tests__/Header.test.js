import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import Header from "../Header"
import {fireEvent, render,screen} from "@testing-library/react"
import appStore from "../utils/appStore"
import "@testing-library/jest-dom";


it("should render Header component with login component",() =>{

    render(
    <BrowserRouter>
        <Provider store = {appStore}>
            <Header/>
        </Provider>
    </BrowserRouter>
    );

    const loginButton = screen.getByRole("button", {name : "Login"});
    //const loginButton = screen.getByText("Login");
    
    expect(loginButton).toBeInTheDocument();
});
it("should render Items component Cart items 0 component",() =>{

    render(
    <BrowserRouter>
        <Provider store = {appStore}>
            <Header/>
        </Provider>
    </BrowserRouter>
    );

   // const cartItems = screen.getByText("Cart - (0 items)");
    const cartItems = screen.getByText(/Cart/);
    
    expect(cartItems).toBeInTheDocument();
});

it("should change to login button to logout on click",() =>{

    render(
    <BrowserRouter>
        <Provider store = {appStore}>
            <Header/>
        </Provider>
    </BrowserRouter>
    );

    const loginButton = screen.getByRole("button",{name : "Login"});
    fireEvent.click(loginButton);

    const logoutBotton = screen.getByRole("button",{name: "Logout"})
    expect(logoutBotton).toBeInTheDocument();
});