import { fireEvent, render, screen } from "@testing-library/react"
import Body from "../Body"
import MOCK_DATA from "../mocks/resListMock.json"
import { BrowserRouter } from "react-router-dom"
import { act } from "react-dom/test-utils"
import "@testing-library/jest-dom"


global.fetch = jest.fn(()=>{
    return Promise.resolve({
        json: () => {
            return Promise.resolve(MOCK_DATA);
        },
    })
})

it("Should render the body component with Search", async()=>{
    await act(async () =>
        render(
            <BrowserRouter>
            <Body/>
            </BrowserRouter>
        )
    );

    const searchBtn = screen.getByRole("button", {name : "Search" });
    
    expect(searchBtn).toBeInTheDocument();
})

it("Should render the body component with Search items", async()=>{
    await act(async () =>
        render(
            <BrowserRouter>
            <Body/>
            </BrowserRouter>
        )
    );

    const searchBtn = screen.getByRole("button", {name : "Search" });

    const searchInput  = screen.getByTestId("searchInput");
    fireEvent.change(searchInput, { target: { value : "Restaurant"}})

    fireEvent.click(searchBtn);
    const cards = screen.getAllByTestId("resCard");
    
    expect(cards.length).toBe(3);
})

it("Should render top rated restaurants", async()=>{
    await act(async () =>
        render(
            <BrowserRouter>
            <Body/>
            </BrowserRouter>
        )
    );
    const cardsBeforeFilter = screen.getAllByTestId("resCard"); 
    expect(cardsBeforeFilter.length).toBe(9);
    const topRatedButton = screen.getByRole("button", { name : "Top Rated Restaurants"})
    fireEvent.click(topRatedButton);
    const cardsAfterFilter = screen.getAllByTestId("resCard")
    expect(cardsAfterFilter.length).toBe(5);
})