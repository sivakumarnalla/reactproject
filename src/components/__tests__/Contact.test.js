import { render,screen } from "@testing-library/react";
import Contact from "../Contact";
import "@testing-library/jest-dom";

test("should load contact us component",()=>{

    render(<Contact/>);
    const heading = screen.getByRole("heading");

    //Assertion
    expect(heading).toBeInTheDocument();
})
test("should load button inside the contact component",()=>{

    render(<Contact/>);
    const button = screen.getByRole("button");

    //Assertion
    expect(button).toBeInTheDocument();
})

test("should load input name inside the contact component",()=>{

    render(<Contact/>);
    const name = screen.getByPlaceholderText("name");

    //Assertion
    expect(name).toBeInTheDocument();
})

test("should load 2 input boxes in the contact component",()=>{

    render(<Contact/>);
    //Quriying
    const inputboxes = screen.getAllByRole("textbox");

    //Assertion
    expect(inputboxes.length).toBe(2);
})