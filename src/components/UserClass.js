import React from "react";

class UserClass extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            userInfo:{
                name:"aaaa",
                location: "default",
            }
           
        }
    }
    async componentDidMount(){
        const data = await fetch("https://api.github.com/users/kumar");

        const json = await data.json();

        this.setState({
            userInfo:json,
        });

    }
    componentDidUpdate(){
        console.log("componentDidUpdate will call");
    }
    componentWillUnmount(){
        console.log("Component will unmount call")
    }
    render(){
        const {name,location,avatar_url} =this.state.userInfo;
         return(
            <div className="user-card m-4 p-4 bg-gray-100 rounded-lg">
                <img src={avatar_url}/>
                <h2>Name : {name}</h2>
                <h3>Location: {location}</h3>
                <h4>contact: 7288983258</h4>
            </div>
        )
    }
    
}
export default UserClass