import Shimmer from "./utils/shimmer";
import { useParams } from "react-router";
import useProductMenu from "./utils/useProductMenu";
import { useState,useEffect } from "react";
import RestaurantCategory from "./RestaurantCategory";

const ProductMenu = () =>{
    const {id} = useParams();
     const [resInfo,setResInfo] = useState(null)
     const [showIndex,setShowIndex]  = useState(0);

    useEffect(() =>{
        fetchProduct();
    },[])

    const fetchProduct = async () =>{
        const data = await fetch("https://www.swiggy.com/dapi/menu/pl?page-type=REGULAR_MENU&complete-menu=true&lat=17.37240&lng=78.43780&restaurantId="+id);
        const json = await data.json();
        setResInfo(json.data);
    }
    //const productInfo = useProductMenu(id);
    if(resInfo === null) 
        return (<Shimmer/>)

        const {name,cuisines,costForTwoMessage} = resInfo?.cards[0]?.card?.card?.info;
        const {itemCards} = resInfo?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards[2]?.card?.card;

        const categories = resInfo?.cards[2]?.groupedCard?.cardGroupMap?.REGULAR?.cards.filter
        ((c)=> c.card?.card?.["@type"] === "type.googleapis.com/swiggy.presentation.food.v2.ItemCategory")

        //console.log(categories);
    
    return(
        
        <div className="text-center">
           <h1 className="font-bold text-2xl py-6">{name}</h1>
           <p className="font-bold">{cuisines.join(",")} - {costForTwoMessage}</p>
            <h2 className="py-4 text-xl font-bold">Menu</h2>
            <ul>
                {
                    // itemCards.map((item) =>(
                    //     <li key={item.card.info.id}>{item.card.info.name} -{"Rs"}
                    //     {item.card.info.defaultPrice/100 || item.card.info.price/100}</li>
                    // ))
                    categories.map((category,index) =>(<RestaurantCategory 
                        key={category?.card?.card.title}
                         data = {category?.card?.card}
                         showItems = {index === showIndex ? true :false}
                         setShowIndex = {() => setShowIndex(index)}
                         />
                         
                    ))
                   
                    
                }
            </ul>


        </div>
    );
}

export default ProductMenu