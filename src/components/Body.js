import { useContext, useEffect, useState } from "react"
import RestaurantCard, { withPromotedLabel } from "./RestaurantCard"
import Shimmer from "./utils/shimmer";
import { Link } from "react-router-dom";
import useOnlineStatus from "./utils/useOnlineStatus";
import UserContext from "./utils/UserContext";


const Body = () =>{
    const [productList,setProductList] = useState([]);
    const [searchText,setSearchText] = useState("");
    const [filterProduct,setFilterProduct] = useState([])
    const onlineStatus = useOnlineStatus();
    const {setUserName,loggedInUser} = useContext(UserContext);

    const RestaurantCardPromoted = withPromotedLabel(RestaurantCard);
    //whenever state variable update,react trigger a reconciliation cycle(re render the component)
    useEffect(()=>{
        fetchData();
    }, []);
    const fetchData = async ()=>{
        const data = await fetch("https://www.swiggy.com/dapi/restaurants/list/v5?lat=17.37240&lng=78.43780&is-seo-homepage-enabled=true&page_type=DESKTOP_WEB_LISTING");
        const json = await data.json();
        setProductList(json?.data?.cards[4]?.card?.card?.gridElements?.infoWithStyle?.restaurants)
        setFilterProduct(json?.data?.cards[4]?.card?.card?.gridElements?.infoWithStyle?.restaurants)
        console.log(json);
    }

    
    //conditional Rendaring
    if(productList.length === 0){
        return (<Shimmer/>)
    }

    if(onlineStatus === false){
        return <h1>Some thing went wrong Please check your internet connection</h1>
    }
    
    return(
        <div className="body">
           <div className="flex items-center">

                {/* Search Bar*/}
                <div className="p-4 m-4">
                    <input type="text" data-testid = "searchInput"
                    className="border border-solid border-black" value={searchText} 
                    onChange={(e)=>setSearchText(e.target.value)}/>
                    <button className ="px-4 py-2 bg-green-100 m-4 rounded-lg"onClick={()=>{
                        const filteredProducts = productList.filter((res)=>res.info.name.toLowerCase().includes(searchText.toLowerCase()))
                        setFilterProduct(filteredProducts);
                    }}>Search</button>
                </div>
                <div className="p-4 m-4">
                    {/* Top Rated Restaurants Code**/}
                <button className="px-4  py-2 m-2  bg-red-300 rounded-xl "
                 onClick={() =>{
                    const filterList = productList.filter(
                        (res) =>res.info.avgRating>4.2
                    );

                    setFilterProduct(filterList);
                }}>Top Rated Restaurants</button>

                </div>

                <div className="p-4 m-4">
                   UserName : <input className="m-2 p-2 border-spacing-0" value={loggedInUser} onChange={(e)=> setUserName(e.target.value)}/>

                </div>

                
           </div>

            <div className="flex flex-wrap">
                {
                    filterProduct.map((restaurant) =>(
                       <Link key = {restaurant.info.id} to = {"/restaurant/"+restaurant.info.id}>
                        {
                            restaurant.info.isOpen ?<RestaurantCardPromoted resData={restaurant}/>: <RestaurantCard  resData={restaurant} />
                        } 
                       </Link>
                    ))
                    
                }
            </div>
        </div>
    
    )
}

export default Body