import { useContext, useState } from "react"
import { LOGO_URL } from "./utils/constant"
import { Link } from "react-router-dom"
import useOnlineStatus from "./utils/useOnlineStatus"
import UserContext from "./utils/UserContext"
import { useSelector } from "react-redux"


const Header = () =>{

    let btnName = "Login"
    const [btnNameReact,setBtnNameReact] = useState("Login")
    const onLineStatus = useOnlineStatus();
    const {loggedInUser} = useContext( UserContext);
    //Subscribing the store using selector
    const cartItems = useSelector((store) =>store.cart.items);

    return(
        <div className="flex justify-between bg-pink-100 mb-2">
            <div className="logo-container">
                <img className="w-40" src="https://www.logodesign.net/logo/smoking-burger-with-letturce-3624ld.png"/>
            </div>
            <div className="flex items-center">
                <ul className="flex p-4 m-4">
                    <li className="px-4">Online Status :{onLineStatus ? "✅" :"🔴"}</li>
                    <li className="px-4"> <Link to="/">Home</Link></li>
                    <li className="px-4"><Link to= "/contact">Contact</Link></li>
                    <li className="px-4"> <Link to="/about">About</Link></li>
                    <li className="px-4"> <Link to= "/grocery">Grocery</Link></li>
                    <li className="px-4 font-bold text-xl" ><Link to= "/cart">Cart - ({cartItems.length} items)</Link></li>
                    <li className="px-4"><button className="login" onClick={()=>{
                       btnNameReact ==="Login"? setBtnNameReact("Logout") : setBtnNameReact("Login") 
                    }}>{btnNameReact}</button></li>
                    <li className="px-4 font-bold">{loggedInUser}</li>
                </ul>
            </div>
        </div>
    )
}

export default Header