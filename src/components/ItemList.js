import { useDispatch } from "react-redux"
import { addItem } from "./utils/cartSlice";


const ItemList = ({items}) =>{
    console.log(items);
   const dispatch = useDispatch();
   const handleAddItems = (item)=>{
            dispatch(addItem(item));
   }

    return(
        <div>
            {items.map((item)=>(
               <div data-testid = "foodItems"
                key={item.card.info.id} className="p-2 m-2 border-gray-200 border-b-2 text-left">
                    <div className="py-2 flex justify-between ">
                        <div>
                        <span>{item.card.info.name}</span>
                        <p>₹ {item.card.info.price/100 ||item.card.info.defaultPrice/100}</p>
                        <p className="text-xs">{item.card.info.description}</p>
                        </div>
                        <div>
                        <img src={"https://media-assets.swiggy.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_660/"+item.card.info.imageId}className="w-20 absolute"/>
                        <button 
                        className="px-3 mx-5 my-10 bg-white text-green-400 rounded-lg shadow-lg   absolute" onClick={() =>handleAddItems(item)}>ADD
                        </button>
                        </div>       
                        
                    </div>
                    
               </div> 
            )) }
        </div>
    )
}

export default ItemList